// Include gulp
var gulp = require('gulp');
// Define base folders
var src = 'src/';
var dest = 'dest/';
// Include plugins
var plugins = require("gulp-load-plugins")({
    pattern: ['gulp-*', 'gulp.*', 'main-bower-files'],
    replaceString: /\bgulp[\-.]/
});

// Compile CSS from Sass files
gulp.task('sass', function(cb) {
    return gulp.src(src + 'scss/style.scss')
        //.pipe(plugins.rename({suffix: '.min'}))
        .pipe(plugins.sass())
        .pipe(gulp.dest(src + 'css'));
    cb(err);
});

// Concatenate & Minify CSS (after sass task is done)
gulp.task('css', ['sass'], function() {
    var cssFiles = ['src/css/*'];
    return gulp.src(plugins.mainBowerFiles().concat(cssFiles))
        .pipe(plugins.filter('*.css'))
        .pipe(plugins.concat('style.css'))
        .pipe(plugins.minifyCss())
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(gulp.dest(dest + 'css'));
});

// Concatenate & Minify JS
gulp.task('js', function() {
    var jsFiles = ['src/js/*'];
    gulp.src(plugins.mainBowerFiles().concat(jsFiles))
        .pipe(plugins.filter('*.js'))
        .pipe(plugins.concat('data.js'))
        .pipe(plugins.concat('main.js'))
        .pipe(plugins.uglify())
        .pipe(plugins.rename({suffix: '.min'}))
        .pipe(gulp.dest(dest + 'js'));
});

// Watch for changes in files
gulp.task('watch', function() {
    // Watch .js files
    gulp.watch(src + 'js/*.js', ['js']);
    // Watch .scss files
    gulp.watch(src + 'scss/*.scss', ['css']);
});
// Default Task
gulp.task('default', ['sass','css','js', 'watch']);