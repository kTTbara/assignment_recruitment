var jsonData = {
    "Profiili": [
        {
            "id": "p1",
            "name": "Age"
        },
        {
            "id": "p0",
            "name": "Gender"
        },
        {
            "id": "p2",
            "name": "Current location"
        },
        {
            "id": "p3",
            "name": "Personal description"
        },
        {
            "id": "p4",
            "name": "Skills"
        },
        {
            "id": "p5",
            "name": "Target sallary"
        },
        {
            "id": "p6",
            "name": "Preferred line of work"
        },
        {
            "id": "p7",
            "name": "Preferred title"
        },
        {
            "id": "p8",
            "name": "Preferred employment type"
        },
        {
            "id": "p9",
            "name": "Preferred work type"
        },
        {
            "id": "p10",
            "name": "Work experience"
        },
        {
            "id": "p11",
            "name": "Education"
        },
        {
            "id": "p12",
            "name": "Certification and Tests results"
        },
        {
            "id": "p13",
            "name": "References"
        }
    ],

    "Generic": [
        {
            "id": "g0",
            "name": "Name"
        },
        {
            "id": "g1",
            "name": "Application arrival date"
        },
        {
            "id": "g2",
            "name": "Application valid until"
        },
        {
            "id": "g3",
            "name": "Recruitment job title"
        },
        {
            "id": "g4",
            "name": "Moved to recruitment"
        },
        {
            "id": "g5",
            "name": "Include direct or moved"
        },
        {
            "id": "g6",
            "name": "Application status"
        },
        {
            "id": "g7",
            "name": "Application classification"
        },
        {
            "id": "g8",
            "name": "Application custom classification 1"
        },
        {
            "id": "g9",
            "name": "Application custom classification 2"
        },
        {
            "id": "g10",
            "name": "Application custom classification 3"
        }
    ],

    "AllData": [
        {
            "id": "g0",
            "name": "Lastname, Firstname"
        },
        {
            "id": "g1",
            "name": "25.5.2012"
        },
        {
            "id": "g2",
            "name": "25.6.2012"
        },
        {
            "id": "g3",
            "name": "Recruitment job title name"
        },
        {
            "id": "g4",
            "name": "Moved to recruitment"
        },
        {
            "id": "g5",
            "name": "Include direct or moved"
        },
        {
            "id": "g6",
            "name": "Potential"
        },
        {
            "id": "g7",
            "name": "Application classification"
        },
        {
            "id": "g8",
            "name": "Application custom classification 1"
        },
        {
            "id": "g9",
            "name": "Application custom classification 2"
        },
        {
            "id": "g10",
            "name": "Application custom classification 3"
        },
        {
            "id": "p0",
            "name": "Gender"
        },
        {
            "id": "p1",
            "name": "23"
        },
        {
            "id": "p2",
            "name": "Kolobrzeg"
        },
        {
            "id": "p3",
            "name": "Personal description"
        },
        {
            "id": "p4",
            "name": "HTML 4<br>CSS 4<br>JavaScript 5<br>English 4"
        },
        {
            "id": "p5",
            "name": "9999"
        },
        {
            "id": "p6",
            "name": "Preferred line of work name"
        },
        {
            "id": "p7",
            "name": "Preferred title name"
        },
        {
            "id": "p8",
            "name": "Preferred employment type name"
        },
        {
            "id": "p9",
            "name": "Preferred work type name"
        },
        {
            "id": "p10",
            "name": "Work experience"
        },
        {
            "id": "p11",
            "name": "Msc. computer sciences<br>Aalto university"
        },
        {
            "id": "p12",
            "name": "Certification and Tests results"
        },
        {
            "id": "p13",
            "name": "References"
        }
    ]
};