$(document).ready(function () {

    //fill profiili attributes
    var listItems = "";
    for (var i = 0; i < jsonData.Profiili.length; i++) {
        listItems += "<option value='" + jsonData.Profiili[i].id + "'>" + jsonData.Profiili[i].name + "</option>";
    }
    $("#profiili-list").html(listItems);

    //fill results table
    var recruitmentHeader="<thead><tr>";
    recruitmentHeader+='<th><div class="checkbox"><label><input id="selectall" type="checkbox" value=""></label></div></td>';

    for (var j = 0; j < jsonData.Generic.length; j++) {
        recruitmentHeader +="<th>"+ jsonData.Generic[j].name +"</th>"
    }
    for (var k = 0; k < jsonData.Profiili.length; k++) {
        recruitmentHeader +="<th>"+ jsonData.Profiili[k].name +"</th>"
    }
    recruitmentHeader+="</tr></thead>";

    var recruitmentItems="<tbody>";
    for (var l = 0; l<14; l++) {
        recruitmentItems+="<tr>";
        recruitmentItems+='<td><div class="checkbox"><label><input class="check" type="checkbox" value="" ></label></div></td>';

        for (var m = 0; m < jsonData.AllData.length; m++) {
            recruitmentItems +="<td>"+ jsonData.AllData[m].name +"</td>"
        }

    }
    recruitmentItems+="</tr></tbody>";


    $("#table-results").html(recruitmentHeader+recruitmentItems);
    console.log(recruitmentHeader);

    //filter
    var checkboxesGeneric = "<p>Generic attributes</p>";
    for (var n = 0; n < jsonData.Generic.length; n++) {
        checkboxesGeneric +='<div class="checkbox"><label><input type="checkbox" value="">'
            +jsonData.Generic[n].name
            +'</label></div>';
    }
    $("#filter-generic").html(checkboxesGeneric);

    var checkboxesProfiili = "<p>Profiili fields</p>";
    for (var o = 0; o < jsonData.Profiili.length; o++) {
        checkboxesProfiili +='<div class="checkbox"><label><input type="checkbox" value="">'
            +jsonData.Profiili[o].name
            +'</label></div>';
    }
    $("#filter-profiili").html(checkboxesProfiili);

    var checkboxesQuestions = "<p>Predefined questions</p>";
    for (var p = 0; p < 14; p++) {
        checkboxesQuestions +='<div class="checkbox"><label><input type="checkbox" value="">'
            +"Predefined question"
            +'</label></div>';
    }
    $("#filter-questions").html(checkboxesQuestions);


    //filter button
    $('#show-filter').click(function(){
        $('#config').hide();
        $('#filter').toggle();
    });

    //config button
    $('#show-config').click(function(){
        $('#filter').hide();
        $('#config').toggle();
    });

    //remove item
    $(".item-remove").click(function(){
        $(this).parent().remove();
    });

    $(document).on('change', '#selectall', function() {
        $(".check").prop('checked', $(this).prop("checked"));
    });
});
